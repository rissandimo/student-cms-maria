package com.rissandimo.studentcms.dao;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Instructor
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String firstName;
    private String lastName;
    private int instructorNumber;

    @OneToMany
    private List<Course> courseList = new ArrayList<>();

    // view students enrolled in classes
}
