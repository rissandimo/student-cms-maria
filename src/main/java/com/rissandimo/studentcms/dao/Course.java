package com.rissandimo.studentcms.dao;

import lombok.Data;

import javax.persistence.*;
import java.util.*;

@Data
@Entity
public class Course
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private int currentNumberOfStudents;
    private int maxNumOfStudents;

    public Course()
    {
    }

    public Course(String name, int maxNumOfStudents)
    {
        this.name = name;
        this.maxNumOfStudents = maxNumOfStudents;
    }

    @ManyToMany(mappedBy = "courseList") // variable name in Student for the arrayList
    private List<Student> enrolledStudents = new ArrayList<>();

    public void addStudent(Student newStudent)
    {
        if(currentNumberOfStudents < maxNumOfStudents)
        {
            enrolledStudents.add(newStudent);
            newStudent.addCourse(this);
            currentNumberOfStudents++;
        }
    }

    @Override
    public String toString()
    {
        return getId() + " : " + name;
    }

}
